# Copyright 2013 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2016 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require launchpad alternatives flag-o-matic

SUMMARY="PBZIP2 is a parallel implementation of the bzip2 block-sorting file compressor"
DESCRIPTION="
PBZIP2 uses pthreads and achieves near-linear speedup on SMP machines. The output of
this version is fully compatible with bzip2 v1.0.2 or newer ie: anything compressed
with pbzip2 can be decompressed with bzip2.
"

LICENCES="BSD-4"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    parts:
        binaries
        documentation
"

DEPENDENCIES="
    build:
        app-arch/bzip2[parts:development][parts:libraries]
"

BUGS_TO="kimrhh@exherbo.org"

src_prepare() {
    # Force usage of pbzip2 parallelism feature
    append-flags '-DIGNORE_TRAILING_GARBAGE=0'
    edo sed -e "s/CXXFLAGS =/CXXFLAGS ?=/" -i Makefile
    edo sed -e 's/PRIuMAX/ & /' -i pbzip2.cpp
    default
}

src_install() {
    local alternatives=()

    if option parts:binaries;then
        dobin pbzip2
        dosym pbzip2 /usr/$(exhost --target)/bin/pbunzip2
        dosym pbzip2 /usr/$(exhost --target)/bin/pbzcat
        alternatives+=( /usr/$(exhost --target)/bin/bzip2      pbzip2
                        /usr/$(exhost --target)/bin/bunzip2    pbunzip2
                        /usr/$(exhost --target)/bin/bzcat      pbzcat   )
    fi
    if option parts:documentation;then
        doman pbzip2.1
        emagicdocs
        alternatives+=( /usr/share/man/man1/bzip2.1            pbzip2.1 )
    fi

    [[ "${#alternatives[@]}" -gt 0 ]] && alternatives_for "bzip2" "${PN}" "100" "${alternatives[@]}"
}

