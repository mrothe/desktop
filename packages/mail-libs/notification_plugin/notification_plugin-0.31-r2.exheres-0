# Copyright 2009, 2010 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

SUMMARY="claws-mail plugin to notify the user of new and unread email"
DESCRIPTION="
The Notification plugin provides various ways to notify the user of new
and possibly unread mail. Currently, the following modules are
implemented:

 * A system tray icon, optionally with popup on new mail arrival
 * A popup window
 * A mail banner (stocks ticker-like widget)
 * Support for a command to be issued on new mail arrival
 * Support for displaying mail status on a LCD
"
HOMEPAGE="http://www.claws-mail.org/plugin.php?plugin=notification"
DOWNLOADS="http://www.claws-mail.org/downloads/plugins/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="libnotify
indicators [[ description = [ Use libindicate to send dbus indicators for new mail ] ]]
libcanberra [[ description = [ Use libcanberra to play audio notifications for new mail ] ]]"

DEPENDENCIES="
    build:
        dev-util/pkg-config
        sys-devel/gettext
    build+run:
        dev-libs/glib:2[>=2.15.6]
        mail-client/claws-mail[>=3.9.0]
        x11-libs/gtk+:2[>=2.10.0]
        libnotify? ( x11-libs/libnotify[>=0.4.3] )
        libcanberra? ( media-libs/libcanberra[>=0.6][providers:gtk2] )
        indicators? ( dev-libs/libindicate[>=0.2.3] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-nls )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    libnotify
    'libcanberra libcanberra-gtk'
    'indicators indicate'
)

