# Copyright 2012 Michael Thomas <aelmalinka@gmail.com>
# Copyright 2016 Arnaud Lefebvre <a.lefebvre@outlook.fr>
# Distributed under the terms of the GNU General Public Licesnse v2

SUMMARY="Google V8 Javascript engine"
DESCRIPTION="
Implements ECMAScript as specified in ECMA-262, 3rd Edition, run
standalone or embedded into any C++ Application
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"

# revisions for SCM dependencies
# These deps can be found in the DEPS file at the root of the v8 project
build_revision="9338ce52d0b9bcef34c38285fbd5023b62739fac"
gyp_revision="d61a9397e668fa9843c4aa7da9e79460fe590bfb"
icu_revision="741688ebf328da9adc52505248bf4e2ef868722c"
buildtools_revision="505de88083136eefd056e5ee4ca0f01fe9b33de8"
trace_event_revision="0e9a47d74970bee1bbfc063c47215406f8918699"
gtest_revision="6f8a66431cb592dad629028a50b3dd418a408c87"
gmock_revision="0421b6f358139f02e102c9c332ce19a33faf75be"
clang_revision="8688d267571de76a56746324dcc249bf4232b85a"

# SCM dependencies
scm_base="https://chromium.googlesource.com/"
scm_deps=( "build" "buildtools" "clang" "gmock" "gtest" "gyp" "icu" "trace_event" )

scm_deps_repositories=( "chromium/src/build" "chromium/buildtools"
  "chromium/src/tools/clang" "external/googlemock" "external/github.com/google/googletest"
  "external/gyp" "chromium/deps/icu" "chromium/src/base/trace_event/common" )

scm_deps_unpack_to=( "build" "buildtools" "tools/clang" "testing/gmock"
  "testing/gtest" "build/gyp" "third_party/icu" "base/trace_event/common" )

SCM_NO_AUTOMATIC_FINALISE=1
SCM_NO_PRIMARY_REPOSITORY="true"
SCM_SECONDARY_REPOSITORIES="${scm_deps[@]}"

require scm-git github [ user=v8 ]

for (( i=0; i<${#scm_deps[@]}; i++ )); do
    SCM_THIS=${scm_deps[$i]}
    scm_dep_revision=${SCM_THIS}_revision
    scm_set_var REPOSITORY "${scm_base}${scm_deps_repositories[$i]}.git"
    scm_set_var REVISION ${!scm_dep_revision}
    scm_set_var UNPACK_TO "${WORK}/${scm_deps_unpack_to[$i]}"
done

scm_finalise

MYOPTIONS="
    platform:
        x86
        amd64
"

DEPENDENCIES="
    build:
        dev-lang/python:*[<3.0]
        dev-libs/icu:=
        dev-python/Jinja2[python_abis:2.7]
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}/fix-gcc7.1-build.patch" )

v8_platform() {
    if option "platform:amd64" ; then
        echo "x64"
    elif option "platform:x86" ; then
        echo "ia32"
    fi
}

src_unpack() {
    default

    scm_src_unpack
}

src_prepare() {
    edo sed -i -e 's:#!/usr/bin/env python:#!/usr/bin/env python2:' tools/run-tests.py
    edo sed -i -e 's/python/python2/' \
        build/gyp/gyp \
        Makefile

    edo ln -s "${WORK}/build/gyp" "${WORK}/tools/gyp"

    default
}

src_compile() {
    GYP_DEFINES="use_system_icu=1 clang=0 snapshot=off" \
    GYPFLAGS="-Dv8_use_external_startup_data=0 -Dlinux_use_bundled_gold=0" \
    AR_host=${AR} \
    CC_host=${CC} \
    CXX_host=${CXX} \
    emake \
        werror=no \
        "library=shared" \
        $(v8_platform).release
}

src_install() {
    insinto /usr/$(exhost --target)
    doins -r include

    pushd "${WORK}/out/$(v8_platform).release"

    pushd "lib.target"
    dolib libv8.so libv8_libbase.so libv8_libplatform.so
    popd

    dobin d8
    popd

    emagicdocs
}

src_test() {
    local cctest=test/cctest/cctest.status

    # These tests are failing as of v5.4.171
    local skip_tests=(
        test-run-machops/RunFloat64MulAndFloat64Add1
        test-run-machops/RunFloat64MulAndFloat64Add2
        test-run-machops/RunFloat64MulAndFloat64Sub1
        test-run-machops/RunFloat64MulAndFloat64Sub2
    )
    local skip_tests_conf=""

    for test in "${skip_tests[@]}"; do
        skip_tests_conf+="'${test}': [SKIP],"
    done

    # Remove the closing ] and add our failing tests
    # to the list of skipped tests for our platform
    edo sed -i 's/^\]$//' $cctest
cat << EOF >> $cctest
    ['arch==$(v8_platform)', {
    $skip_tests_conf
    }]]
EOF

    # Some tests need an US locale
    LC_ALL=en_US.UTF-8 tools/run-tests.py --arch $(v8_platform) --mode release

    assert "Tests failed"
}
